Name: Ferro Geraldi Hardian

NPM: 1706028612

Class: Advanced Programming - B


## Links
1. DDP 2 https://gitlab.com/ferrogh/DDP2-Assignments.git
2. Exercise https://gitlab.com/ferrogh/my-first-git-repo.git

## My Notes
1. Git Branch Usage
    1. Why use git branch?
        - Because I want to experiment without changing the existing files in master branch
    2. How?
        - create new branch advpro-tutorial then make all changes there
2. Git Revert Usage
    1. Why use git revert?
        - I deleted readme files in the repo for fun and push it. But then I want to undo the changes I made. So I use git revert
    2. How?
        - find the commit id with git log, then use git revert


